import React from 'react';

export default class Home extends React.Component{
    render() {
        return(
            <div className={"container" } style={{paddingTop:'10vw'}}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="text-contact">
                            <h2 className="text-contact">ติดต่อ KRUTRANSFORM</h2>
                            <h2>บริษัท ไวส์ เอ็ดดูเคชี่น เซ็นเตอร์ จำกัด Wise Education Center </h2>
                            <h2>ส่งเมลหาเรา</h2>
                        </div>

                        <form action="" >
                            <div className="kru-input">
                                <div className="row">
                                    <input type="text" placeholder={"ชื่อ"}/>
                                    <input type="text" placeholder={"นามสกุล"}/>
                                </div>
                                <div className="row">
                                    <input type="text" placeholder={"ข้อความ"}/>
                                </div>
                            </div>
                            <button>Submit</button>
                        </form>



                    </div>
                    <div className="col-md-6">
                        <div className={""}>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}