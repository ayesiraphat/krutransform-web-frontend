import React from 'react';
import Home from '../Components/Home'
import About from '../Components/About'
import Gallery from '../Components/Gallery'
import Contact from '../Components/Contact'

export default class Nav extends React.Component{
    render() {
        const $ =(window).$;
        $(window).on('scroll',function () {
            if($(window).scrollTop()){
                $('nav.navbar').addClass('black');
            }
            else {
                $('nav.navbar').removeClass('black');
            }
        })

        function myFunction() {
            var x = document.getElementById("kru-nav");
            if (x.className === "navbar") {
                x.className += " responsive";
            } else {
                x.className = "navbar";
            }
        }
        return(
                <div className={""}>
                    <div className="">
                        <div className="" id="home">
                            <Home/>
                        </div>
                    </div>
                    <div className={"row"}>
                    <div className={"col-md-12"}>
                        <nav className="navbar " id={"kru-nav"} >
                            <ul className="nav menu-wrapper">
                                <li><a href="#about">ABOUT US</a></li>
                                <li><a href="#course">COURSE</a></li>
                            </ul>
                            <li>
                                <a href="#home">
                                    <div className="logo-wrapper" >
                                        <span className="triangle-down"> </span>
                                        <img src="/Images/logo.png"
                                             alt="" className={"logo"}/>
                                    </div>
                                </a>
                            </li>
                            <ul className="menu-wrapper nav">
                                <li><a href="#gallery">GALLERY</a></li>
                                <li><a href="#contact">CONTACT</a></li>
                            </ul>

                        </nav>
                    </div>

                </div>
                <div>
                    <div>
                        <div className="kru-about" id="about">
                            <About/>
                        </div>
                        <div className={"kru-about-profile"}>
                            {/*<img src="https://public-v2links.adobecc.com/c517d6e8-c0a7-4f46-6e24-550977e1bda3/component?params=component_id:c3e00818-ecb2-44bd-a9ce-8bebf95ef02b&params=version:0&token=1562663516_da39a3ee_3b3bc02493575e1a12192eb3b9d7ffff6fb45586&api_key=CometServer1" alt=""/>*/}
                        </div>

                    </div>


                    <div className="kru-course" id="course">
                        <h1>THIS IS Course</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

                    </div>
                    <div className="kru-gallery" id="gallery">
                       <h1 className="blue-about">ภาพบรรยากาศ<span className="text-about">KRUTRANSFORM</span></h1>
                        <Gallery/>
                    </div>
                    <div className="kru-contact" id="contact">
                        <Contact />
                    </div>
               </div>
                <div >

                </div>
                    <img src="https://public-v2links.adobecc.com/c517d6e8-c0a7-4f46-6e24-550977e1bda3/component?params=component_id:9c99be5d-501f-4726-9495-606036646dd5&params=version:0&token=1562278558_da39a3ee_8f38315f2ffdc5a397ba8dfb25f68b9d249ca5a2&api_key=CometServer1"
                             alt="" id={"iconline"}/>
                         <img src="https://public-v2links.adobecc.com/c517d6e8-c0a7-4f46-6e24-550977e1bda3/component?params=component_id:2cbc401f-0dfd-4ec6-857b-000e8c0cfb15&params=version:0&token=1562278558_da39a3ee_8f38315f2ffdc5a397ba8dfb25f68b9d249ca5a2&api_key=CometServer1"
                         alt="" id={"iconfc"}/>
                </div>
        )
    }

}