import React from 'react';
import '../slider-animations.css';
import '../styles.css';
import {Animated} from "react-animated-css";
import {delay} from "q";

export default class Home extends React.Component{
    render() {

        return(
            <div>
                <div >
                    <div id="carouselExampleIndicators" className="carousel slide slide" data-ride="carousel">

                        <div className="carousel-inner ">
                            <div className="carousel-item active">
                                <div className="banner">
                                    {/*<img src="https://public-v2links.adobecc.com/c517d6e8-c0a7-4f46-6e24-550977e1bda3/component?params=component_id:680f8e50-e852-47cf-80e8-0ef29414a50e&params=version:0&token=1562278558_da39a3ee_8f38315f2ffdc5a397ba8dfb25f68b9d249ca5a2&api_key=CometServer1"*/}
                                         {/*alt="" id="bg"/>*/}
                                    <img src="/Images/pic1.png" alt="" id={"bg"}/>
                                </div>
                                <div className="carousel-caption trickcenter">
                                    <h2 className="animated bounceInLeft"
                                    style={{animationDelay:'1s'}}>KRUTRANSFORM </h2>
                                    <h3 className="animated bounceInLeft"
                                        style={{animationDelay:'2s'}}>มากประสบการณ์</h3>
                                    <h4 className="animated bounceInRight"
                                        style={{animationDelay:'2s'}}>KRUTRANSFORM มีประสบการณ์มากกว่า 18 ปี ในการสอนนักเรียนห้องเรียนพิเศษในโรงเรียนชั้นนำ อาทิเช่น โรงเรียนมนเครืออัสสัมชัน เซนหลุยส์ระยอง จัดโครงการติว O-net รวมไปถึงจัดทำค่ายวิทยาศาสตร์ให้กับนักเรียนพิเศษอีกมากมาย</h4>
                                    <h5 className="animated zoomInDown"
                                        style={{animationDelay:'3s'}}><a href="">คลิ๊กเลย!</a></h5>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="banner">
                                    {/*<img src="https://public-v2links.adobecc.com/c517d6e8-c0a7-4f46-6e24-550977e1bda3/component?params=component_id:71357025-bc58-4c50-8303-2fffb76b7870&params=version:0&token=1562278558_da39a3ee_8f38315f2ffdc5a397ba8dfb25f68b9d249ca5a2&api_key=CometServer1"*/}
                                         {/*alt="" id="bg"/>*/}
                                    <img src="/Images/pic2.png" alt="" id={"bg"}/>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="banner">
                                    {/*<img src="https://public-v2links.adobecc.com/c517d6e8-c0a7-4f46-6e24-550977e1bda3/component?params=component_id:71357025-bc58-4c50-8303-2fffb76b7870&params=version:0&token=1562278558_da39a3ee_8f38315f2ffdc5a397ba8dfb25f68b9d249ca5a2&api_key=CometServer1"*/}
                                    {/*alt="" id="bg"/>*/}
                                    <img src="/Images/pic3.png" alt="" id={"bg"}/>
                                </div>
                            </div>
                            {/*<div className="carousel-item">*/}
                                {/*<div className="banner">*/}
                                    {/*<img src="https://public-v2links.adobecc.com/c517d6e8-c0a7-4f46-6e24-550977e1bda3/component?params=component_id:a968a3ee-7cb2-43d4-b0cf-29b9bfa70a30&params=version:0&token=1562278558_da39a3ee_8f38315f2ffdc5a397ba8dfb25f68b9d249ca5a2&api_key=CometServer1"*/}
                                         {/*alt="" id="bg"/>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                            <ol className="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                        </div>
                        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                           data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button"
                           data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </div>




        )
    }

}