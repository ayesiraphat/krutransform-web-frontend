import React from 'react';

export default class Home extends React.Component{
    render() {
        return(
            <div className="container  ">
               <h2 className={"text-about"}>เกี่ยวกับ KRUTRANSFORM</h2>
                <hr/>
                <div className={"row"}>
                    <div className={"col-md-6"}>
                      <h2 className="blue-about" >วิสัยทัศน์</h2>
                        <h1 className="text-about-small " >อินดอร์ เวิร์กบิ๊กมาร์จินลอร์ดเอาต์ เวอร์โอเพ่นปิยมิตรมลภาวะ แฮนด์รีวิวอุปทาน ซิงโรลออนสปอร์ต ครัวซองโต๊ะจีนเซนเซอร์โฮสเตสพฤหัส เซ็นเตอร์ยิมแฟรนไชส์ซะ เรตติ้ง แบดคอลัมนิสต์รีสอร์ตเซ็นทรัลอพาร์ทเมนต์ ไลน์เมจิก ซิมโฟนีไลน์แคปผลักดัน ต้าอ่วยฮ็อตด็อกรีสอร์ตมอลล์ แครกเกอร์สเตย์โอเปร่า ไคลแม็กซ์คลิปกุนซือพอเพียง ไฮเทคราชานุญาตพันธุวิศวกรรม คอร์ปอเรชั่นพาสตา เซ่นไหว้บาลานซ์ ม้าหินอ่อน เอาต์ปัจเจกชนโปรโมชั่นแกงค์ สเตย์วาฟเฟิล โปรเจกเตอร์ปอดแหกพุทธภูมิ เซ็กซี่ มัฟฟินมะกันไกด์ แคร็กเกอร์แมชชีนแตงกวา พาสตาคอร์ปอเรชั่นเรซิน แบล็คสตรอเบอรีโจ๋รองรับ บาลานซ์วิภัชภาคร็อคเป่ายิ้งฉุบไบเบิล เรซินฟรังก์แคชเชียร์ตู้เซฟ หมายปองบาร์บีคิวปักขคณนาซูเปอร์แดนซ์ โยเกิร์ตคอร์ปอเรชั่นแต๋ว เวิร์คอินเตอร์ห่วยสแตนเลส กู๋แหม็บฮิปฮอปรัมสต็อค</h1>
                    </div>
                    <div className={"col-md-6"}>
                        <h2 className="blue-about">พันธกิจ</h2>
                        <h1 className="text-about-small ">อินดอร์ เวิร์กบิ๊กมาร์จินลอร์ดเอาต์ เวอร์โอเพ่นปิยมิตรมลภาวะ แฮนด์รีวิวอุปทาน ซิงโรลออนสปอร์ต ครัวซองโต๊ะจีนเซนเซอร์โฮสเตสพฤหัส เซ็นเตอร์ยิมแฟรนไชส์ซะ เรตติ้ง แบดคอลัมนิสต์รีสอร์ตเซ็นทรัลอพาร์ทเมนต์ ไลน์เมจิก ซิมโฟนีไลน์แคปผลักดัน ต้าอ่วยฮ็อตด็อกรีสอร์ตมอลล์ แครกเกอร์สเตย์โอเปร่า ไคลแม็กซ์คลิปกุนซือพอเพียง ไฮเทคราชานุญาตพันธุวิศวกรรม คอร์ปอเรชั่นพาสตา เซ่นไหว้บาลานซ์ ม้าหินอ่อน เอาต์ปัจเจกชนโปรโมชั่นแกงค์ สเตย์วาฟเฟิล โปรเจกเตอร์ปอดแหกพุทธภูมิ เซ็กซี่ มัฟฟินมะกันไกด์ แคร็กเกอร์แมชชีนแตงกวา พาสตาคอร์ปอเรชั่นเรซิน แบล็คสตรอเบอรีโจ๋รองรับ บาลานซ์วิภัชภาคร็อคเป่ายิ้งฉุบไบเบิล เรซินฟรังก์แคชเชียร์ตู้เซฟ หมายปองบาร์บีคิวปักขคณนาซูเปอร์แดนซ์ โยเกิร์ตคอร์ปอเรชั่นแต๋ว เวิร์คอินเตอร์ห่วยสแตนเลส กู๋แหม็บฮิปฮอปรัมสต็อค</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="card" style={{width:' 18rem'}}>
                            <div className={"card-img-top"}>
                                {/*<img src="https://public-v2links.adobecc.com/c517d6e8-c0a7-4f46-6e24-550977e1bda3/component?params=component_id:953a60d3-75e4-44f6-8261-6bf9c1306a61&params=version:0&token=1562278558_da39a3ee_8f38315f2ffdc5a397ba8dfb25f68b9d249ca5a2&api_key=CometServer1"*/}
                                     {/*alt="" id="card"/>*/}

                            </div>
                                <div className="card-body">
                                    <p className="card-text">Some quick example text to build on the card title and make
                                        up the bulk of the card's content.</p>
                                </div>
                            </div>

                    </div>
                    <div className="col-md-4">
                        <div className="card" style={{width:' 18rem'}}>
                            <div>

                            </div>
                            <div className="card-body">
                                <ps className="card-text">Some quick example text to build on the card title and make
                                    up the bulk of the card's content.</ps>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card" style={{width:' 18rem'}}>
                            <div>
                            </div>
                            <div className="card-body">
                                <p className="card-text">Some quick example text to build on the card title and make
                                    up the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <h1 className="text-about-small ">
                           ประะยุทธ์ บลาๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆ
                        </h1>
                    </div>
                    <div className="col-md-4">
                        <h1 className="text-about-small ">ธนาธร บลาๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆ
                        </h1>

                    </div>
                    <div className="col-md-4">
                        <h1 className="text-about-small ">หญิงหน่อย บลาๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆๆ

                        </h1>

                    </div>
                </div>

            </div>
        )
    }

}